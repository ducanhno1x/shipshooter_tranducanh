using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditorInternal.ReorderableList;
using static UnityEngine.RuleTile.TilingRuleOutput;
using UnityEngine.UIElements;
using UnityEngine.UI;

namespace Player
{ 
public class shipMove : MonoBehaviour
{
    public float speed = 1f;
    public Sprite Left;
    public Sprite Right;
    public Sprite defaul;
    public Vector2 position;
    public int maxBlood;
    public Text bloodText;
    public int currentBlood;
    public bool isGameOver;
    public GameObject gameover;



    void Start()
    {
        isGameOver = false;
        this.GetComponent<SpriteRenderer>().sprite = defaul;
        maxBlood = 10;
        currentBlood = maxBlood;
        bloodText.text = currentBlood.ToString();
        gameover.GetComponent<SpriteRenderer>().enabled = false;


    }



    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        transform.position = Vector2.MoveTowards(transform.position, position, speed * Time.deltaTime);

        if (position.x < transform.position.x)
        {
            this.GetComponent<SpriteRenderer>().sprite = Right;
        }
        else if (position.x > transform.position.x)
        {
            this.GetComponent<SpriteRenderer>().sprite = Left;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = defaul;
        }
    }
    public void TakeDamage(int damage)
    {
        currentBlood -= damage;
        bloodText.text =   currentBlood.ToString();
        if (currentBlood <= 0)
        {

            Gameover();
        }

    }
    public void Gameover()
    {
        isGameOver = true;
        gameover.GetComponent<SpriteRenderer>().enabled = true;
        Time.timeScale = 0;
        Destroy(gameObject);

    }





} }

