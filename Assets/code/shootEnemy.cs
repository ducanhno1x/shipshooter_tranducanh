﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootEnemy : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float fireRate = 0.5f;
    public float nextFire = 0.0f;
    public GameObject shootPoint1;

    void Update()
    {
        Shoot();
    }

    void Shoot()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            GameObject bullet = Instantiate(bulletPrefab, shootPoint1.transform.position, Quaternion.identity);
            bullet.transform.Rotate(Vector3.forward, 180f); // xoay đối tượng bắn đạn 180 độ theo trục z để đạn bắn ngược lại từ trên xuống
        }
    }
}
