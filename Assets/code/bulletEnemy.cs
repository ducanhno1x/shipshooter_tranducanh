﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletEnemy : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 10f; // tốc độ di chuyển của đạn
    public int damage = 1; // sát thương gây ra khi va chạm với đối tượng khác

    void Update()
    {
        // di chuyển đạn theo hướng trước
        transform.Translate(Vector2.up * speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // Kiểm tra đối tượng va chạm có là enemy hay không
        Player.shipMove ship = other.GetComponent<Player.shipMove>();
        if (ship != null)
        {
            // Gây sát thương cho enemy nếu va chạm
            ship.TakeDamage(damage);
        }

        // Hủy đạn sau khi va chạm
        Destroy(gameObject);
    }
}
