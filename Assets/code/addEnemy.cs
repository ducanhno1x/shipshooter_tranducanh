﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addEnemy : MonoBehaviour
{
    public GameObject enemyPrefab; // Prefab của enemy
    public float spawnRate = 2f; // Tần suất spawn enemy
    public float spawnRadius = 4f; // Bán kính spawn enemy

    void Start()
    {
        
        InvokeRepeating("SpawnEnemy", 0f, spawnRate);
    }

    void SpawnEnemy()
    {
        // Tạo một instance mới của enemy prefab
        GameObject enemyInstance = Instantiate(enemyPrefab);

        // Đặt vị trí ngẫu nhiên cho enemy instance
        Vector2 randomPosition = Random.insideUnitCircle.normalized * spawnRadius;
        enemyInstance.transform.position = new Vector3(randomPosition.x, randomPosition.y, 0f);
    }
}

