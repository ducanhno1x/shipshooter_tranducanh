﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMove : MonoBehaviour
{

    public float speed;
    public GameObject[] taget;
    private GameObject m_currenTaget;
    private int m_currentIndex;
    void Start()
    {
        // thiết lập vị trí ban đầu cho enemy
        m_currentIndex= 0;
    }

    void Update()
    {
        
        m_currenTaget = taget[m_currentIndex];
        if (Vector3.Distance(transform.position, m_currenTaget.transform.position) < 0.1f)
        {
            m_currentIndex++;
            if(m_currentIndex > taget.Length - 1)
            {
                m_currentIndex = 0;
            }
            m_currenTaget = taget[m_currentIndex];
        }
        else
        {

        }
        transform.position = Vector3.Lerp(transform.position, m_currenTaget.transform.position, speed);
    }
}

