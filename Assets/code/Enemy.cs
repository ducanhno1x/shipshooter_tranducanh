﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Mygame
{
    public class Enemy : MonoBehaviour
    {
        
        public int maxHealth = 3; // máu tối đa của enemy
        public int currentHealth; // máu hiện tại của enemy
        public int score ;
        public Text scoreText;


        void Start()
        {
            currentHealth = maxHealth;
            score=0;
           scoreText.text=  score.ToString();

        }

        public void TakeDamage(int damage)
        {
            currentHealth -= damage;
            if (currentHealth <= 0)
            {
                Die();     

            }
        }

        void Die()
        {
            // Hủy enemy và hiệu ứng khi enemy bị tiêu diệt
            Destroy(gameObject);
            score +=1;
            scoreText.text = score.ToString();


        }
    }
}
